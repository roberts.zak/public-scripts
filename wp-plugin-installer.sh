#!/bin/sh
ifCheck=$1;
pluginUrl=$2;
for d in $(find ~/ -maxdepth 1 -type d)
do
	if [ -d "$d$ifCheck" ]; then
		wp --path="$d/public/" --force plugin install $pluginUrl --activate
	fi
done
