#!/bin/bash

if [ ! -f /usr/local/bin/wp ]; then
    cd ~
    curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
    chmod 755 ~/wp-cli.phar
    mv ~/wp-cli.phar /usr/local/bin/wp
fi
